namespace Pottencial.Payment.Core.Extensions;

public static class StringExtensions
{
    public static bool OnlyNumbers(this string str)
    {
        return str.All(char.IsDigit);
    }
}