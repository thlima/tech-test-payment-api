﻿using Pottencial.Payment.Core.DomainObjects;

namespace Pottencial.Payment.Domain
{
    public class Seller : Entity
    {
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Cpf { get; private set; }
        public string Phone { get; private set; }

        public Seller()
        {

        }

        public Seller(string name, string email, string cpf, string phone)
        {
            Name = name;
            Email = email;
            Cpf = cpf;
            Phone = phone;
        }
    }
}
