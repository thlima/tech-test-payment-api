﻿using Pottencial.Payment.Core.DomainObjects;

namespace Pottencial.Payment.Domain
{
    public class UserAuthentication : Entity, IAggregateRoot
    {
        public string Email { get; private set; }
        public string Password { get; private set; }

        public UserAuthentication(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }
}
