namespace Pottencial.Payment.Domain.Repository;

public interface ISaleRepository
{
    Task<Sale?> GetByIdAsync(Guid id, CancellationToken cancellationToken);
    Task AddAsync(Sale sale, CancellationToken cancellationToken);
    Task AddItemSaleAsync(Sale sale, List<Product> products, CancellationToken cancellationToken);
    Task UpdateAsync(Sale sale, CancellationToken cancellationToken);
    Task<List<Product>> GetAllProductsBySaleId(Guid saleId);
    Task RemoveProductAsync(Sale sale, Product product, CancellationToken cancellationToken);
}