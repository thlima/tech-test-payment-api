﻿namespace Pottencial.Payment.Domain.Repository
{
    public interface IUserAuthenticationRepository
    {
        Task AddAsync(UserAuthentication userAuthentication);
        Task<UserAuthentication> GetUserByEmailAndPasswordAsync(string email, string passwordHash);
    }
}
