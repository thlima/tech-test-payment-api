using Pottencial.Payment.Core.DomainObjects;
using Pottencial.Payment.Domain.Enums;

namespace Pottencial.Payment.Domain;

public class Sale : Entity, IAggregateRoot
{
    public Seller Seller { get; private set; }
    public Guid SellerId { get; set; }
    public DateTime SaleDate { get; private set; }
    public List<Product> Products { get; private set; }
    public SaleStatus Status { get; private set; }

    public Sale()
    {

    }

    public Sale(Seller seller)
    {
        Products = new List<Product>();
        Seller = seller;
        SellerId = seller.Id;

        SaleDate = DateTime.Now;
        Status = SaleStatus.AwaitingPayment;
    }

    public void AddProduct(Product product)
    {
        product.SetSale(this);
        Products.Add(product);
    }

    public bool ValidateAndUpdateSaleStatus(SaleStatus currentStatus, SaleStatus newStatus)
    {
        var transactions = new Dictionary<SaleStatus, List<SaleStatus>>
        {
            { SaleStatus.AwaitingPayment, new List<SaleStatus> { SaleStatus.PaymentAccept, SaleStatus.Canceled } },
            { SaleStatus.PaymentAccept, new List<SaleStatus> { SaleStatus.SentToCarrier, SaleStatus.Canceled } },
            { SaleStatus.SentToCarrier, new List<SaleStatus> { SaleStatus.Delivered } }
        };

        if (!transactions.ContainsKey(currentStatus) || !transactions[currentStatus].Contains(newStatus))
            return false;

        UpdateStatus(newStatus);

        return true;
    }

    private void UpdateStatus(SaleStatus status)
    {
        Status = status;
    }
}