namespace Pottencial.Payment.Domain.Notifications;

public interface INotifier
{
    bool HasNotification();

    List<Notification> GetNotifications();

    void Handle(Notification notification);
}