﻿using Pottencial.Payment.Domain.Enums;

namespace Pottencial.Payment.Domain.Services
{
    public interface ISaleService
    {
        Task<Sale> AddSaleAsync(Sale sale, CancellationToken cancellationToken);
        Task AddSaleItemAsync(Guid saleId, List<Product> products, CancellationToken cancellationToken);
        Task<bool> UpdateSaleStatusAsync(Guid id, SaleStatus status, CancellationToken cancellationToken);
        Task<bool> RemoveSaleItemAsync(Guid saleId, Guid productId, CancellationToken cancellationToken);
    }
}
