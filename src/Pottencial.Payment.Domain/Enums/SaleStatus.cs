﻿namespace Pottencial.Payment.Domain.Enums
{
    public enum SaleStatus
    {
        AwaitingPayment = 0,
        PaymentAccept = 1,
        SentToCarrier = 2,
        Delivered = 3,
        Canceled = 4
    }

    public static class ESaleStatusHelper
    {
        public static SaleStatus ConvertFromInt(int value)
        {
            if (Enum.IsDefined(typeof(SaleStatus), value))
                return (SaleStatus)value;

            throw new ArgumentOutOfRangeException(nameof(value), value, "This status does not exists");
        }
    }
}
