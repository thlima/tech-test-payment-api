﻿using Pottencial.Payment.Core.DomainObjects;
using System.Text.Json.Serialization;

namespace Pottencial.Payment.Domain
{
    public class Product : Entity
    {
        public string Description { get; private set; }
        public Guid SaleId { get; private set; }

        [JsonIgnore]
        public Sale Sale { get; private set; }

        public Product()
        {

        }

        public Product(string productDescription)
        {
            Description = productDescription;
        }

        public void SetSale(Sale sale)
        {
            Sale = sale;
            SaleId = sale.Id;
        }
    }
}
