using AutoMapper;
using Pottencial.Payment.Domain;
using Pottencial.Payment.WebApi.Dtos;

namespace Pottencial.Payment.WebApi.Configuration;

public class AutoMapperConfig : Profile
{
    public AutoMapperConfig()
    {
        CreateMap<Sale, SaleDto>().ReverseMap();
        CreateMap<Product, ProductDto>().ReverseMap();
        CreateMap<Seller, SellerDto>().ReverseMap();
    }
}