using MediatR;
using Pottencial.Payment.Application.CommandStack.AddSale.Responses;
using Pottencial.Payment.Application.CommandStack.AddSaleItem;
using Pottencial.Payment.Application.CommandStack.CreateUserAuthentication;
using Pottencial.Payment.Application.CommandStack.LoginUserAuthentication;
using Pottencial.Payment.Application.CommandStack.LoginUserAuthentication.Responses;
using Pottencial.Payment.Application.CommandStack.RemoveSaleItem;
using Pottencial.Payment.Application.QueryStack.GetSaleById;
using Pottencial.Payment.Application.QueryStack.GetSaleById.Responses;
using Pottencial.Payment.CommandStack.AddSale;
using Pottencial.Payment.CommandStack.UpdateSaleStatus;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Repository;
using Pottencial.Payment.Domain.Services;
using Pottencial.Payment.Infrastructure;
using Pottencial.Payment.Infrastructure.Notifications;
using Pottencial.Payment.Infrastructure.Repository;
using Pottencial.Payment.Infrastructure.Services;

namespace Pottencial.Payment.WebApi.Configuration;

public static class DependencyInjectionConfig
{
    public static IServiceCollection ResolveDependecies(this IServiceCollection services)
    {
        services.AddScoped<PottencialPaymentContext>();
        
        //Repositories
        services.AddScoped<ISaleRepository, SaleRepository>();
        services.AddScoped<IUserAuthenticationRepository, UserAuthenticationRepository>();
        
        //Handlers (Sales)
        services.AddScoped<IRequestHandler<AddSaleCommand, AddSaleCommandResponse>, AddSaleCommandHandler>();
        services.AddScoped<IRequestHandler<AddSaleItemCommand, Unit>, AddSaleItemCommandHandler>();
        services.AddScoped<IRequestHandler<UpdateSaleStatusCommand, bool>, UpdateSaleStatusCommandHandler>();
        services.AddScoped<IRequestHandler<GetSaleByIdQuery, GetSaleByIdQueryResponse>, GetSaleByIdQueryHandler>();
        services.AddScoped<IRequestHandler<RemoveSaleItemCommand, bool>, RemoveSaleItemCommandHandler>();

        //Handlers (UserAuthentication)
        services.AddScoped<IRequestHandler<CreateUserAuthenticationCommand, Guid>, CreateUserAuthenticationCommandHandler>();
        services.AddScoped<IRequestHandler<LoginUserAuthenticationCommand, LoginUserAuthenticationResponse>, LoginUserAuthenticationCommandHandler>();
        
        //Services
        services.AddScoped<ISaleService, SaleService>();
        services.AddScoped<IAuthService, AuthService>();
        
        //Notifier
        services.AddScoped<INotifier, Notifier>();
        
        return services;
    }
}