﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Application.CommandStack.CreateUserAuthentication;
using Pottencial.Payment.Application.CommandStack.LoginUserAuthentication;
using Pottencial.Payment.Domain.Notifications;

namespace Pottencial.Payment.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class UserAuthenticationController : MainController
    {
        private readonly IMediator _mediator;

        public UserAuthenticationController(INotifier notifier, IMediator mediator) : base(notifier)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateUserAuthenticationCommand command)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            var id = await _mediator.Send(command);

            return CustomResponse(id);
        }

        [HttpPut("login")]
        public async Task<IActionResult> Login([FromBody] LoginUserAuthenticationCommand command)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            var loginUserAuthenticationResponse = await _mediator.Send(command);

            return CustomResponse(loginUserAuthenticationResponse);
        }
    }
}
