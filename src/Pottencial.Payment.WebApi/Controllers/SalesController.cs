﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Application.QueryStack.GetSaleById;
using Pottencial.Payment.CommandStack.AddSale;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain;
using Pottencial.Payment.WebApi.Dtos;
using AutoMapper;
using Pottencial.Payment.Application.CommandStack.AddSaleItem;
using Pottencial.Payment.Application.CommandStack.RemoveSaleItem;
using Pottencial.Payment.CommandStack.UpdateSaleStatus;
using Microsoft.AspNetCore.Authorization;

namespace Pottencial.Payment.WebApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class SalesController : MainController
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public SalesController(IMediator mediator,
            IMapper mapper,
            INotifier notifier) : base(notifier)
        {
            _mapper = mapper;
            _mediator = mediator;
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(new GetSaleByIdQuery(id), cancellationToken);

            return CustomResponse(result);
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SaleDto saleDto, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            var sale = _mapper.Map<Sale>(saleDto);

            var result = await _mediator.Send(new AddSaleCommand(sale), cancellationToken);

            return CustomResponse(result);
        }

        [HttpPost("items/{saleId:guid}")]
        public async Task<IActionResult> PostItemAsync(Guid saleId, [FromBody] List<ProductDto> productDto, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            var products = _mapper.Map<List<Product>>(productDto);

            await _mediator.Send(new AddSaleItemCommand(saleId, products), cancellationToken);

            return CustomResponse();
        }

        [HttpPut("{saleId:Guid}/{newSaleStatus:int}")]
        public async Task<IActionResult> PutAsync(Guid saleId, int newSaleStatus, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            var salesStatusConverted = ESaleStatusHelper.ConvertFromInt(newSaleStatus);

            var result = await _mediator.Send(new UpdateSaleStatusCommand(saleId, salesStatusConverted), cancellationToken);

            return CustomResponse(new
            {
                saleIsUpdated = result
            });
        }

        [HttpDelete("items/{saleId}/{productId}")]
        public async Task<IActionResult> DeleteItemSaleAsync(Guid saleId, Guid productId, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return CustomResponse(ModelState);

            var saleRemoved = await _mediator.Send(new RemoveSaleItemCommand(saleId, productId), cancellationToken);

            return CustomResponse(new
            {
                saleIsRemoved = saleRemoved
            });
        }
    }
}
