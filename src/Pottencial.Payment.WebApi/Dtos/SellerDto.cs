namespace Pottencial.Payment.WebApi.Dtos;

public class SellerDto
{
    public string Name { get; set; }
    public string Email { get;  set; }
    public string Cpf { get;  set; }
    public string Phone { get;  set; }
}