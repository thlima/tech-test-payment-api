namespace Pottencial.Payment.WebApi.Dtos;

public class SaleDto
{
    public SellerDto Seller { get; set; }
    public List<ProductDto> Products { get; set; }
}