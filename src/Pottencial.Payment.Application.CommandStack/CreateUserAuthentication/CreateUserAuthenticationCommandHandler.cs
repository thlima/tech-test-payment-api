﻿using MediatR;
using Pottencial.Payment.Domain;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Repository;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.Application.CommandStack.CreateUserAuthentication
{
    public class CreateUserAuthenticationCommandHandler : IRequestHandler<CreateUserAuthenticationCommand, Guid>
    {
        private readonly IUserAuthenticationRepository _userAuthenticationRepository;
        private readonly IAuthService _authService;
        private readonly INotifier _notifier;

        public CreateUserAuthenticationCommandHandler(IUserAuthenticationRepository userAuthenticationRepository, IAuthService authService, INotifier notifier)
        {
            _userAuthenticationRepository = userAuthenticationRepository;
            _authService = authService;
            _notifier = notifier;
        }

        public async Task<Guid> Handle(CreateUserAuthenticationCommand command, CancellationToken cancellationToken)
        {
            if (!ValidateCommand(command))
            {
                command.ValidationResult?.Errors.ForEach(error => _notifier.Handle(new Notification(error.ErrorMessage)));

                return Guid.Empty;
            }

            var passwordHash = _authService.ComputeSha256Hash(command.Password);

            var user = new UserAuthentication(command.Email, passwordHash);

            await _userAuthenticationRepository.AddAsync(user);

            return user.Id;
        }

        private static bool ValidateCommand(CreateUserAuthenticationCommand command)
        {
            return command.IsValid();
        }
    }
}
