﻿using FluentValidation.Results;
using MediatR;
using Pottencial.Payment.Application.CommandStack.CreateUserAuthentication.Validations;
using System.Text.Json.Serialization;

namespace Pottencial.Payment.Application.CommandStack.CreateUserAuthentication
{
    public class CreateUserAuthenticationCommand : IRequest<Guid>
    {
        [JsonIgnore]
        public ValidationResult? ValidationResult { get; private set; }

        public string Email { get; set; }
        public string Password { get; set; }

        public bool IsValid()
        {
            ValidationResult = new CreateUserAuthenticationValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }
}
