﻿using FluentValidation;

namespace Pottencial.Payment.Application.CommandStack.CreateUserAuthentication.Validations
{
    public class CreateUserAuthenticationValidation : AbstractValidator<CreateUserAuthenticationCommand>
    {
        public CreateUserAuthenticationValidation()
        {
            RuleFor(p => p.Email)
                .EmailAddress()
                .WithMessage("Email not valid!");

            RuleFor(p => p.Password)
                .MinimumLength(5)
                .WithMessage("Password must contain at least 5 characters");
        }
    }
}
