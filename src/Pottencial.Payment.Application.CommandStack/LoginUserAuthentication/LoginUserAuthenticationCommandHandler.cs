﻿using MediatR;
using Pottencial.Payment.Application.CommandStack.LoginUserAuthentication.Responses;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Repository;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.Application.CommandStack.LoginUserAuthentication
{
    public class LoginUserAuthenticationCommandHandler : IRequestHandler<LoginUserAuthenticationCommand, LoginUserAuthenticationResponse>
    {
        private readonly IAuthService _authService;
        private readonly IUserAuthenticationRepository _userAuthenticationRepository;
        private readonly INotifier _notifier;

        public LoginUserAuthenticationCommandHandler(IAuthService authService, IUserAuthenticationRepository userAuthenticationRepository, INotifier notifier)
        {
            _authService = authService;
            _userAuthenticationRepository = userAuthenticationRepository;
            _notifier = notifier;
        }

        public async Task<LoginUserAuthenticationResponse> Handle(LoginUserAuthenticationCommand request, CancellationToken cancellationToken)
        {
            var passwordHash = _authService.ComputeSha256Hash(request.Password);

            var user = await _userAuthenticationRepository.GetUserByEmailAndPasswordAsync(request.Email, passwordHash);

            if (user == null)
            {
                _notifier.Handle(new Notification("\r\nUsername or password is incorrect."));
                return null;
            }

            var token = _authService.GenerateJwtToken(user.Email);

            return new LoginUserAuthenticationResponse(user.Email, token);
        }
    }
}
