﻿namespace Pottencial.Payment.Application.CommandStack.LoginUserAuthentication.Responses
{
    public class LoginUserAuthenticationResponse
    {
        public string Email { get; set; }
        public string Token { get; set; }

        public LoginUserAuthenticationResponse(string email, string token)
        {
            Email = email;
            Token = token;
        }
    }
}
