﻿using MediatR;
using Pottencial.Payment.Application.CommandStack.LoginUserAuthentication.Responses;

namespace Pottencial.Payment.Application.CommandStack.LoginUserAuthentication
{
    public class LoginUserAuthenticationCommand : IRequest<LoginUserAuthenticationResponse>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
