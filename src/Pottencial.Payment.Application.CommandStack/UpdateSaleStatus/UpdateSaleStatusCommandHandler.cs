using MediatR;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.CommandStack.UpdateSaleStatus;

public class UpdateSaleStatusCommandHandler : IRequestHandler<UpdateSaleStatusCommand, bool>
{
    private readonly ISaleService _saleService;

    public UpdateSaleStatusCommandHandler(ISaleService saleService)
    {
        _saleService = saleService;
    }

    public Task<bool> Handle(UpdateSaleStatusCommand request, CancellationToken cancellationToken)
    {
        var result = _saleService.UpdateSaleStatusAsync(request.Id, request.Status, cancellationToken);

        return result;
    }
}