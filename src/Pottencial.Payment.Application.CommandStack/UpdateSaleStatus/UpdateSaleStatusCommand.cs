using MediatR;
using Pottencial.Payment.Domain.Enums;

namespace Pottencial.Payment.CommandStack.UpdateSaleStatus;

public class UpdateSaleStatusCommand : IRequest<bool>
{
    public Guid Id { get; set; }
    public SaleStatus Status { get; set; }
    
    public UpdateSaleStatusCommand(Guid id, SaleStatus status)
    {
        Id = id;
        Status = status;
    }
}