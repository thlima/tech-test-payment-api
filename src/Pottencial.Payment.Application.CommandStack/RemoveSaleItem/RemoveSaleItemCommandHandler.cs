﻿using MediatR;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.Application.CommandStack.RemoveSaleItem
{
    public class RemoveSaleItemCommandHandler : IRequestHandler<RemoveSaleItemCommand, bool>
    {
        public readonly ISaleService _saleService;

        public RemoveSaleItemCommandHandler(ISaleService saleService)
        {
            _saleService = saleService;
        }

        public async Task<bool> Handle(RemoveSaleItemCommand request, CancellationToken cancellationToken)
        {
            var result = await _saleService.RemoveSaleItemAsync(request.SaleId, request.ProductId, cancellationToken);

            return result;
        }
    }
}
