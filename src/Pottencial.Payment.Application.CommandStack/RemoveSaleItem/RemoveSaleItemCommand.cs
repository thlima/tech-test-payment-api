﻿using MediatR;

namespace Pottencial.Payment.Application.CommandStack.RemoveSaleItem
{
    public class RemoveSaleItemCommand : IRequest<bool>
    {
        public Guid SaleId { get; set; }
        public Guid ProductId { get; set; }

        public RemoveSaleItemCommand(Guid saleId, Guid productId)
        {
            SaleId = saleId;
            ProductId = productId;
        }
    }
}
