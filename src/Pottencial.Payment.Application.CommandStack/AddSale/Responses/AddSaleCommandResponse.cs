﻿namespace Pottencial.Payment.Application.CommandStack.AddSale.Responses
{
    public class AddSaleCommandResponse
    {
        public Guid SaleId { get; set; }
        public Guid SellerId { get; set; }
    }
}
