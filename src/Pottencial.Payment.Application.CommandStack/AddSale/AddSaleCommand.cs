using FluentValidation.Results;
using MediatR;
using Pottencial.Payment.Application.CommandStack.AddSale.Responses;
using Pottencial.Payment.CommandStack.AddSale.Validations;
using Pottencial.Payment.Domain;

namespace Pottencial.Payment.CommandStack.AddSale;

public class AddSaleCommand : IRequest<AddSaleCommandResponse>
{
    public ValidationResult? ValidationResult { get; private set; }
    public Sale Sale { get; set; }

    public AddSaleCommand(Sale sale)
    {
        Sale = sale;
    }

    public bool IsValid()
    {
        ValidationResult = new AddSaleValidation().Validate(this);

        return ValidationResult.IsValid;
    }
}