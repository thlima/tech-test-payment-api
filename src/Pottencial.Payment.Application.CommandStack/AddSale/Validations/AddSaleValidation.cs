using FluentValidation;
using Pottencial.Payment.Core.Extensions;

namespace Pottencial.Payment.CommandStack.AddSale.Validations;

public class AddSaleValidation : AbstractValidator<AddSaleCommand>
{
    public AddSaleValidation()
    {
        RuleFor(s => s.Sale.Products)
            .NotEmpty()
            .WithMessage("The sale cannot be registered without products");
        
        RuleFor(s => s.Sale.Seller.Cpf)
            .Must(StringExtensions.OnlyNumbers)
            .WithMessage("The cpf field of seller needs to be entered with numbers only");
        
        RuleFor(s => s.Sale.Seller.Phone)
            .Must(StringExtensions.OnlyNumbers)
            .WithMessage("The phone field of seller needs to be entered with numbers only");
    }
}