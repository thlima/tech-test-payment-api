using MediatR;
using Pottencial.Payment.Application.CommandStack.AddSale.Responses;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.CommandStack.AddSale;

public class AddSaleCommandHandler : IRequestHandler<AddSaleCommand, AddSaleCommandResponse>
{
    private readonly ISaleService _saleService;
    private readonly INotifier _notifier;

    public AddSaleCommandHandler(ISaleService saleService, INotifier notifier)
    {
        _saleService = saleService;
        _notifier = notifier;
    }

    public async Task<AddSaleCommandResponse> Handle(AddSaleCommand command, CancellationToken cancellationToken)
    {
        if (!ValidateCommand(command))
        {
            command.ValidationResult?.Errors.ForEach(error => _notifier.Handle(new Notification(error.ErrorMessage)));
            
            return null;
        }
        
        var sale = await _saleService.AddSaleAsync(command.Sale, cancellationToken);

        var result = new AddSaleCommandResponse
        {
            SaleId = sale.Id,
            SellerId = sale.SellerId
        };

        return result;
    }

    private static bool ValidateCommand(AddSaleCommand command)
    {
        return command.IsValid();
    }
}
