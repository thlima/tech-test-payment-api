﻿using FluentValidation;

namespace Pottencial.Payment.Application.CommandStack.AddSaleItem.Validations
{
    public class AddSaleItemValidation : AbstractValidator<AddSaleItemCommand>
    {
        public AddSaleItemValidation()
        {
            RuleFor(x => x.Products.Select(x => x.Description))
                .NotNull()
                .NotEmpty()
                .WithMessage("The filed Products does not be empty");
        }
    }
}
