﻿using FluentValidation.Results;
using MediatR;
using Pottencial.Payment.Application.CommandStack.AddSaleItem.Validations;
using Pottencial.Payment.Domain;

namespace Pottencial.Payment.Application.CommandStack.AddSaleItem
{
    public class AddSaleItemCommand : IRequest<Unit>
    {
        public ValidationResult? ValidationResult { get; private set; }
        public Guid SaleId { get; set; }
        public List<Product> Products { get; set; }

        public AddSaleItemCommand(Guid saleId, List<Product> products)
        {
            SaleId = saleId;
            Products = products;
        }

        public bool IsValid()
        {
            ValidationResult = new AddSaleItemValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }
}
