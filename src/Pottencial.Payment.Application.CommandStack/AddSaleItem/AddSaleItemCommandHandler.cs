﻿using MediatR;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.Application.CommandStack.AddSaleItem
{
    public class AddSaleItemCommandHandler : IRequestHandler<AddSaleItemCommand, Unit>
    {
        private readonly ISaleService _saleService;
        private readonly INotifier _notifier;

        public AddSaleItemCommandHandler(ISaleService saleService, INotifier notifier)
        {
            _saleService = saleService;
            _notifier = notifier;
        }

        public async Task<Unit> Handle(AddSaleItemCommand command, CancellationToken cancellationToken)
        {
            if (!ValidateCommand(command))
            {
                command.ValidationResult?.Errors.ForEach(error => _notifier.Handle(new Notification(error.ErrorMessage)));

                return Unit.Value;
            }

            await _saleService.AddSaleItemAsync(command.SaleId, command.Products, cancellationToken);

            return Unit.Value;
        }

        private static bool ValidateCommand(AddSaleItemCommand command)
        {
            return command.IsValid();
        }
    }
}
