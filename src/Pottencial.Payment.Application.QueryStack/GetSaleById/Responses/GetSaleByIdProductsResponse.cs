﻿namespace Pottencial.Payment.Application.QueryStack.GetSaleById.Responses
{
    public class GetSaleByIdProductsResponse
    {
        public Guid ProductId { get; set; }
        public Guid SaleId { get; set; }
        public string Description { get; set; }
    }
}
