﻿using Pottencial.Payment.Domain;

namespace Pottencial.Payment.Application.QueryStack.GetSaleById.Responses
{
    public class GetSaleByIdQueryResponse
    {
        public string SellerName { get; set; }
        public Guid SellerId { get; set; }
        public DateTime SaleDate { get; set; }
        public List<GetSaleByIdProductsResponse> Products { get; set; }
        public string Status { get; set; }
    }
}
