﻿using MediatR;
using Pottencial.Payment.Application.QueryStack.GetSaleById.Responses;

namespace Pottencial.Payment.Application.QueryStack.GetSaleById
{
    public class GetSaleByIdQuery : IRequest<GetSaleByIdQueryResponse>
    {
        public Guid Id { get; set; }

        public GetSaleByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
