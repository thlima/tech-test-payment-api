﻿using MediatR;
using Pottencial.Payment.Application.QueryStack.GetSaleById.Responses;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Repository;

namespace Pottencial.Payment.Application.QueryStack.GetSaleById
{
    public class GetSaleByIdQueryHandler : IRequestHandler<GetSaleByIdQuery, GetSaleByIdQueryResponse>
    {
        private readonly ISaleRepository _saleRepository;
        private readonly INotifier _notifier;

        public GetSaleByIdQueryHandler(ISaleRepository saleRepository, INotifier notifier)
        {
            _saleRepository = saleRepository;
            _notifier = notifier;
        }

        public async Task<GetSaleByIdQueryResponse> Handle(GetSaleByIdQuery request, CancellationToken cancellationToken)
        {
            var sale = await _saleRepository.GetByIdAsync(request.Id, cancellationToken);
            var products = await _saleRepository.GetAllProductsBySaleId(request.Id);

            List<GetSaleByIdProductsResponse> responseProductResult = new();

            if (sale == null)
            {
                _notifier.Handle(new Notification("Sale was not found!"));
                return null;
            }

            foreach (var product in products)
            {
                var responseProduct = new GetSaleByIdProductsResponse
                {
                    ProductId = product.Id,
                    SaleId = product.SaleId,
                    Description = product.Description,
                };

                responseProductResult.Add(responseProduct);
            }
            
            return new GetSaleByIdQueryResponse()
            {
                SellerName = sale.Seller.Name,
                SellerId = sale.Seller.Id,
                SaleDate = sale.SaleDate,
                Status = sale.Status.ToString(),
                Products = responseProductResult
            };
        }
    }
}
