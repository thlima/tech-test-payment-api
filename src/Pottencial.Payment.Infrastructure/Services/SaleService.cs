using Pottencial.Payment.Domain;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Repository;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.Infrastructure.Services;

public class SaleService : ISaleService
{
    private readonly ISaleRepository _saleRepository;
    private readonly INotifier _notifier;

    public SaleService(ISaleRepository saleRepository, INotifier notifier)
    {
        _saleRepository = saleRepository;
        _notifier = notifier;
    }

    public async Task<Sale> AddSaleAsync(Sale sale, CancellationToken cancellationToken)
    {
        await _saleRepository.AddAsync(sale, cancellationToken);

        var saleResult = await _saleRepository.GetByIdAsync(sale.Id, cancellationToken);

        if (saleResult != null)
            return saleResult;

        _notifier.Handle(new Notification("Sale was not found!"));

        return null;
    }

    public async Task AddSaleItemAsync(Guid saleId, List<Product> products, CancellationToken cancellationToken)
    {
        var sale = await _saleRepository.GetByIdAsync(saleId, cancellationToken);

        if (sale != null)
        {
            var errorMessage = "It is not possible to add items to a sale that is not in 'Awaiting Payment' status.";
            NotifyIfSaleIsNotAwaitingPaymentStatus(sale, errorMessage);

            if (!_notifier.GetNotifications().Any())
                await _saleRepository.AddItemSaleAsync(sale, products, cancellationToken);
        }
        else
        {
            _notifier.Handle(new Notification("Sale was not found!"));
        }
    }

    public async Task<bool> UpdateSaleStatusAsync(Guid id, SaleStatus status, CancellationToken cancellationToken)
    {
        var sale = await _saleRepository.GetByIdAsync(id, cancellationToken);

        if (sale == null)
        {
            _notifier.Handle(new Notification("Sale was not found!"));

            return false;
        }
        var updateStatusIsValid = sale.ValidateAndUpdateSaleStatus(sale.Status, status);

        if (!updateStatusIsValid)
        {
            _notifier.Handle(new Notification($"Cannot change status from {sale.Status} to {status}"));

            return false;
        }

        await _saleRepository.UpdateAsync(sale, cancellationToken);

        return true;
    }

    public async Task<bool> RemoveSaleItemAsync(Guid saleId, Guid productId, CancellationToken cancellationToken)
    {
        var sale = await _saleRepository.GetByIdAsync(saleId, cancellationToken);

        if (sale == null)
        {
            _notifier.Handle(new Notification("Sale was not found!"));

            return false;
        }

        if (sale.Products.Count == 1)
        {
            _notifier.Handle(new Notification("It is not possible to remove this product, as a sale must have at least one product"));

            return false;
        }

        var product = sale.Products.FirstOrDefault(p => p.Id == productId);

        if (product == null)
        {
            _notifier.Handle(new Notification("Product not found in the sale!"));
            return false;
        }

        var errorMessage = "It is not possible to remove items to a sale that is not in 'Awaiting Payment' status.";
        NotifyIfSaleIsNotAwaitingPaymentStatus(sale, errorMessage);

        if (_notifier.GetNotifications().Any())
            return false;

        await _saleRepository.RemoveProductAsync(sale, product, cancellationToken);

        return true;
    }

    private void NotifyIfSaleIsNotAwaitingPaymentStatus(Sale sale, string errorMessage)
    {
        if (sale.Status != SaleStatus.AwaitingPayment)
            _notifier.Handle(new Notification(errorMessage));
    }
}