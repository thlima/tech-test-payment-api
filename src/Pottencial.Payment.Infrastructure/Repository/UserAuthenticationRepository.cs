﻿using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain;
using Pottencial.Payment.Domain.Repository;

namespace Pottencial.Payment.Infrastructure.Repository
{
    public class UserAuthenticationRepository : IUserAuthenticationRepository
    {
        private readonly PottencialPaymentContext _context;

        public UserAuthenticationRepository(PottencialPaymentContext context)
        {
            _context = context;
        }

        public async Task AddAsync(UserAuthentication userAuthentication)
        {
            await _context.UsersAuthentication.AddAsync(userAuthentication);
            await _context.SaveChangesAsync();
        }

        public async Task<UserAuthentication> GetUserByEmailAndPasswordAsync(string email, string passwordHash) =>
             await _context.UsersAuthentication?.SingleOrDefaultAsync(x => x.Email == email && x.Password == passwordHash);
    }
}
