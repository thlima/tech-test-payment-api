using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain;
using Pottencial.Payment.Domain.Repository;

namespace Pottencial.Payment.Infrastructure.Repository;

public class SaleRepository : ISaleRepository
{
    private readonly PottencialPaymentContext _context;

    public SaleRepository(PottencialPaymentContext context)
    {
        _context = context;
    }
    
    public async Task<Sale?> GetByIdAsync(Guid id, CancellationToken cancellationToken)
    {
        var sale = await _context.Sales
            .Include(s => s.Seller)
            .Include(s => s.Products)
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        return sale;
    }

    public async Task<List<Product>> GetAllProductsBySaleId(Guid saleId)
    {
        var products = await _context.Products.Where(x => x.SaleId == saleId).ToListAsync();

        return products;
    }

    public async Task AddAsync(Sale sale, CancellationToken cancellationToken)
    {
        _context.Sales.Add(sale);

        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task AddItemSaleAsync(Sale sale, List<Product> products, CancellationToken cancellationToken)
    {
        foreach (var newProduct in products)
        {
            if (!sale.Products.Any(p => p.Description == newProduct.Description))
            {
                sale.AddProduct(newProduct);
                await _context.Products.AddAsync(newProduct, cancellationToken);
            }
        }

        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdateAsync(Sale sale, CancellationToken cancellationToken)
    {
        _context.Sales.Update(sale);
        
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task RemoveProductAsync(Sale sale, Product product, CancellationToken cancellationToken)
    {
        sale.Products.Remove(product);
        _context.Products.Remove(product);

        await _context.SaveChangesAsync();
    }
}