using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain;
using System.Reflection;

namespace Pottencial.Payment.Infrastructure;

public class PottencialPaymentContext : DbContext
{
    public PottencialPaymentContext(DbContextOptions<PottencialPaymentContext> options) : base(options) { }

    public DbSet<Sale> Sales { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<UserAuthentication> UsersAuthentication { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}