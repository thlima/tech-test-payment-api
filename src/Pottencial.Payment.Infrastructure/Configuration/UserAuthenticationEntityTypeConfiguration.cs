﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Payment.Domain;

namespace Pottencial.Payment.Infrastructure.Configuration
{
    public class UserAuthenticationEntityTypeConfiguration : IEntityTypeConfiguration<UserAuthentication>
    {
        public void Configure(EntityTypeBuilder<UserAuthentication> builder)
        {
            builder
                .HasKey(x => x.Id);
        }
    }
}
