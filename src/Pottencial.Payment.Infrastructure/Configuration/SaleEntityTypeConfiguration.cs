﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Payment.Domain;

namespace Pottencial.Payment.Infrastructure.Configuration
{
    public class SaleEntityTypeConfiguration : IEntityTypeConfiguration<Sale>
    {
        public void Configure(EntityTypeBuilder<Sale> builder)
        {

            builder.HasKey(s => s.Id);

            builder.Property(s => s.SaleDate)
                .IsRequired();

            builder.Property(s => s.Status)
                .IsRequired();

            builder.HasOne(s => s.Seller)
                .WithMany()
                .HasForeignKey(s => s.SellerId);

            builder.HasMany(s => s.Products)
                .WithOne(p => p.Sale)
                .HasForeignKey(p => p.SaleId);
        }
    }
}
