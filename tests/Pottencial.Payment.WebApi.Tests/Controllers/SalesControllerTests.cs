﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Pottencial.Payment.Application.QueryStack.GetSaleById.Responses;
using Pottencial.Payment.Application.QueryStack.GetSaleById;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.WebApi.Controllers;
using Pottencial.Payment.CommandStack.AddSale;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain;
using Pottencial.Payment.WebApi.Dtos;
using Pottencial.Payment.Application.CommandStack.AddSale.Responses;
using Pottencial.Payment.CommandStack.UpdateSaleStatus;
using Pottencial.Payment.Application.CommandStack.AddSaleItem;
using Pottencial.Payment.Application.CommandStack.RemoveSaleItem;

namespace Pottencial.Payment.WebApi.Tests.Controllers
{
    public class SalesControllerTests
    {
        private readonly Mock<IMediator> _mediatorMock = new();
        private readonly Mock<IMapper> _mapperMock = new();
        private readonly Mock<INotifier> _notifierMock = new();

        [Fact]
        public async Task GetByIdAsync_ValidId_ReturnsOkObjectResult()
        {
            // Arrange
            var id = Guid.NewGuid();
            var cancellationToken = new CancellationToken();
            var query = new GetSaleByIdQuery(id);
            var expectedResult = new GetSaleByIdQueryResponse();

            _mediatorMock.Setup(mediator => mediator.Send(query, cancellationToken))
                .ReturnsAsync(expectedResult);

            var controller = new SalesController(_mediatorMock.Object, _mapperMock.Object, _notifierMock.Object);

            // Act
            var result = await controller.GetByIdAsync(id, cancellationToken);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task PostAsync_ValidSaleDto_ReturnsOkObjectResult()
        {
            // Arrange
            var saleDto = new SaleDto();
            var cancellationToken = new CancellationToken();
            var sale = new Sale();
            var command = new AddSaleCommand(sale);
            var expectedResult = new AddSaleCommandResponse();

            _mediatorMock.Setup(mediator => mediator.Send(command, cancellationToken))
                .ReturnsAsync(expectedResult);

            var controller = new SalesController(_mediatorMock.Object, _mapperMock.Object, _notifierMock.Object);

            // Act
            var result = await controller.PostAsync(saleDto, cancellationToken);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task PutAsync_ValidSaleIdAndStatus_ReturnsOkObjectResult()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var newSaleStatus = 1;
            var cancellationToken = new CancellationToken();
            var command = new UpdateSaleStatusCommand(saleId, SaleStatus.PaymentAccept);

            _mediatorMock.Setup(mediator => mediator.Send(command, cancellationToken))
                .ReturnsAsync(true);

            var controller = new SalesController(_mediatorMock.Object, _mapperMock.Object, _notifierMock.Object);

            // Act
            var result = await controller.PutAsync(saleId, newSaleStatus, cancellationToken);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task PostItemAsync_ValidSaleIdAndProducts_ReturnOkObjectResult()
        {
            //Arrange
            var saleId = Guid.NewGuid();
            var productDtos = new List<ProductDto>();

            var products = new List<Product>();
            var cancellationToken = new CancellationToken();
            var command = new AddSaleItemCommand(saleId, products);


            _mapperMock.Setup(mapper => mapper.Map<List<Product>>(productDtos)).Returns(products);

            _mediatorMock.Setup(mediator => mediator.Send(command, cancellationToken));

            var controller = new SalesController(_mediatorMock.Object, _mapperMock.Object, _notifierMock.Object);

            //Act
            var result = await controller.PostItemAsync(saleId, productDtos, cancellationToken);

            //Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task DeleteItemSaleAsync_ValidSaleIdAndProductId_ReturnOkObjectResult()
        {
            //Arrange
            var saleId = Guid.NewGuid();
            var productId = Guid.NewGuid();
            var cancellationToken = new CancellationToken();

            var command = new RemoveSaleItemCommand(saleId, productId);
            _mediatorMock.Setup(mediator => mediator.Send(command, cancellationToken));

            var controller = new SalesController(_mediatorMock.Object, _mapperMock.Object, _notifierMock.Object);

            //Act
            var result = await controller.DeleteItemSaleAsync(saleId, productId, cancellationToken);

            //Assert
            Assert.IsType<OkObjectResult>(result);
        }
    }
}
