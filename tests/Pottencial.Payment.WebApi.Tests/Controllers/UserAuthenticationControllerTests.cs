﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Pottencial.Payment.Application.CommandStack.CreateUserAuthentication;
using Pottencial.Payment.Application.CommandStack.LoginUserAuthentication.Responses;
using Pottencial.Payment.Application.CommandStack.LoginUserAuthentication;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.WebApi.Controllers;

namespace Pottencial.Payment.WebApi.Tests.Controllers
{
    public class UserAuthenticationControllerTests
    {
        private readonly Mock<IMediator> _mediatorMock;
        private readonly Mock<INotifier> _notifierMock;
        private readonly UserAuthenticationController _controller;

        public UserAuthenticationControllerTests()
        {
            _mediatorMock = new Mock<IMediator>();
            _notifierMock = new Mock<INotifier>();
            _controller = new UserAuthenticationController(_notifierMock.Object, _mediatorMock.Object);
        }

        [Fact]
        public async Task PostAsync_Valid_ReturnsOkResult ()
        {
            // Arrange
            var command = new CreateUserAuthenticationCommand
            {
                Email = "test@test.com",
                Password = "password"
            };

            // Act
            var result = await _controller.Post(command);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Login_ValidModelState_ReturnsCustomResponse()
        {
            // Arrange
            var command = new LoginUserAuthenticationCommand
            {
                Email = "test@test.com",
                Password = "12345"
            };

            var response = new LoginUserAuthenticationResponse(command.Email, "validToken");

            _mediatorMock.Setup(m => m.Send(It.IsAny<LoginUserAuthenticationCommand>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            // Act
            var result = await _controller.Login(command);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }
    }
}
