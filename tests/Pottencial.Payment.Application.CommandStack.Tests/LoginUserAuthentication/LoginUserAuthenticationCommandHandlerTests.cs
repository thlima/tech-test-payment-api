﻿using Moq;
using Pottencial.Payment.Application.CommandStack.LoginUserAuthentication;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Repository;
using Pottencial.Payment.Domain.Services;
using Pottencial.Payment.Domain;

namespace Pottencial.Payment.Application.CommandStack.Tests.LoginUserAuthentication
{
    public class LoginUserAuthenticationCommandHandlerTests
    {
        private readonly Mock<IAuthService> _authServiceMock;
        private readonly Mock<IUserAuthenticationRepository> _userAuthenticationRepositoryMock;
        private readonly Mock<INotifier> _notifierMock;
        private readonly LoginUserAuthenticationCommandHandler _handler;

        public LoginUserAuthenticationCommandHandlerTests()
        {
            _authServiceMock = new Mock<IAuthService>();
            _userAuthenticationRepositoryMock = new Mock<IUserAuthenticationRepository>();
            _notifierMock = new Mock<INotifier>();

            _handler = new LoginUserAuthenticationCommandHandler(
                _authServiceMock.Object,
                _userAuthenticationRepositoryMock.Object,
                _notifierMock.Object);
        }

        [Fact]
        public async Task Handle_InvalidCredentials_NotifiesErrorAndReturnsNull()
        {
            // Arrange
            var command = new LoginUserAuthenticationCommand
            {
                Email = "invalidemail@example.com",
                Password = "wrongpassword"
            };

            _authServiceMock.Setup(a => a.ComputeSha256Hash(command.Password))
                .Returns("hashedwrongpassword");

            _userAuthenticationRepositoryMock.Setup(r => r.GetUserByEmailAndPasswordAsync(command.Email, "hashedwrongpassword"))
                .ReturnsAsync((UserAuthentication)null);

            _notifierMock.Setup(n => n.Handle(It.IsAny<Notification>()));

            // Act
            var result = await _handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.Null(result);
            _notifierMock.Verify(n => n.Handle(It.IsAny<Notification>()), Times.Once);
        }

        [Fact]
        public async Task Handle_ValidCredentials_ReturnsResponse()
        {
            // Arrange
            var command = new LoginUserAuthenticationCommand
            {
                Email = "validemail@example.com",
                Password = "correctpassword"
            };

            _authServiceMock.Setup(a => a.ComputeSha256Hash(command.Password))
                .Returns("hashedcorrectpassword");

            var user = new UserAuthentication(command.Email, "hashedcorrectpassword");

            _userAuthenticationRepositoryMock.Setup(r => r.GetUserByEmailAndPasswordAsync(command.Email, "hashedcorrectpassword"))
                .ReturnsAsync(user);

            _authServiceMock.Setup(a => a.GenerateJwtToken(command.Email))
                .Returns("validtoken");

            // Act
            var result = await _handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(command.Email, result.Email);
            Assert.Equal("validtoken", result.Token);
        }
    }
}
