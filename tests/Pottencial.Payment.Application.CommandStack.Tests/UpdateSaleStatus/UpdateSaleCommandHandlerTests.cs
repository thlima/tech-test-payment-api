﻿using FluentAssertions;
using Moq;
using Pottencial.Payment.CommandStack.UpdateSaleStatus;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.Application.CommandStack.Tests.UpdateSaleStatus
{
    public class UpdateSaleCommandHandlerTests
    {
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task Handle_WithValidCommand_ShouldReturnIsUpdated(bool isUpdated)
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var status = SaleStatus.AwaitingPayment;
            var command = new UpdateSaleStatusCommand(saleId, status);

            var saleServiceMock = new Mock<ISaleService>();
            saleServiceMock
                .Setup(s => s.UpdateSaleStatusAsync(saleId, status, It.IsAny<CancellationToken>()))
                .ReturnsAsync(isUpdated)
                .Verifiable();

            var handler = new UpdateSaleStatusCommandHandler(saleServiceMock.Object);

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert

            if(isUpdated)
                result.Should().BeTrue();
            else
                result.Should().BeFalse();

            saleServiceMock.Verify(s => s.UpdateSaleStatusAsync(saleId, status, It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
