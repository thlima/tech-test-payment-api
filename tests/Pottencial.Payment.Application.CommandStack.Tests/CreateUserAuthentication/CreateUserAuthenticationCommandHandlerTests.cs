﻿using Moq;
using Pottencial.Payment.Application.CommandStack.CreateUserAuthentication;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Repository;
using Pottencial.Payment.Domain.Services;
using Pottencial.Payment.Domain;

namespace Pottencial.Payment.Application.CommandStack.Tests.CreateUserAuthentication
{
    public class CreateUserAuthenticationCommandHandlerTests
    {
        private readonly Mock<IUserAuthenticationRepository> _userAuthenticationRepositoryMock;
        private readonly Mock<IAuthService> _authServiceMock;
        private readonly Mock<INotifier> _notifierMock;
        private readonly CreateUserAuthenticationCommandHandler _handler;

        public CreateUserAuthenticationCommandHandlerTests()
        {
            _userAuthenticationRepositoryMock = new Mock<IUserAuthenticationRepository>();
            _authServiceMock = new Mock<IAuthService>();
            _notifierMock = new Mock<INotifier>();

            _handler = new CreateUserAuthenticationCommandHandler(
                _userAuthenticationRepositoryMock.Object,
                _authServiceMock.Object,
                _notifierMock.Object);
        }

        [Fact]
        public async Task Handle_InvalidCommand_NotifiesErrorAndReturnsEmptyGuid()
        {
            // Arrange
            var command = new CreateUserAuthenticationCommand
            {
                Email = "invalidemail",
                Password = "password"
            };

            _notifierMock.Setup(n => n.Handle(It.IsAny<Notification>()));

            // Act
            var result = await _handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.Equal(Guid.Empty, result);
            _notifierMock.Verify(n => n.Handle(It.IsAny<Notification>()), Times.Once);
        }

        [Fact]
        public async Task Handle_ValidCommand_AddsUserAndReturnsGuid()
        {
            // Arrange
            var command = new CreateUserAuthenticationCommand
            {
                Email = "validemail@example.com",
                Password = "password"
            };

            _authServiceMock.Setup(a => a.ComputeSha256Hash(command.Password))
                .Returns("hashedpassword");

            var userId = Guid.NewGuid();
            _userAuthenticationRepositoryMock.Setup(r => r.AddAsync(It.IsAny<UserAuthentication>()))
                .Callback<UserAuthentication>(user => user.Id = userId)
                .Returns(Task.CompletedTask);

            // Act
            var result = await _handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.Equal(userId, result);
            _userAuthenticationRepositoryMock.Verify(r => r.AddAsync(It.IsAny<UserAuthentication>()), Times.Once);
        }
    }
}
