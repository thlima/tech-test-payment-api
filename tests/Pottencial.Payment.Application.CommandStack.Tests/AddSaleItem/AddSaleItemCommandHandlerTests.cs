﻿using MediatR;
using Moq;
using Pottencial.Payment.Application.CommandStack.AddSaleItem;
using Pottencial.Payment.Domain.Services;
using Pottencial.Payment.Domain;
using Pottencial.Payment.Domain.Notifications;

namespace Pottencial.Payment.Application.CommandStack.Tests.AddSaleItem
{
    public class AddSaleItemCommandHandlerTests
    {
        [Fact]
        public async Task Handle_WithValidCommand_ShouldCallAddSaleItemAsyncOnce()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var products = new List<Product> { new Product("TV"), new Product("PS5")};
            var command = new AddSaleItemCommand(saleId, products);

            var saleServiceMock = new Mock<ISaleService>();
            var notifierMock = new Mock<INotifier>();

            saleServiceMock
                .Setup(s => s.AddSaleItemAsync(saleId, products, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();

            var handler = new AddSaleItemCommandHandler(saleServiceMock.Object, notifierMock.Object);

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.Equal(Unit.Value, result);
            saleServiceMock.Verify(s => s.AddSaleItemAsync(saleId, products, It.IsAny<CancellationToken>()), Times.Once);
        }

        [Fact]
        public async Task Handle_WithInValidCommand_ShouldCallValidationsAndNotifyError()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var products = new List<Product>();
            var command = new AddSaleItemCommand(saleId, products);

            var saleServiceMock = new Mock<ISaleService>();
            var notifierMock = new Mock<INotifier>();

            saleServiceMock
                .Setup(s => s.AddSaleItemAsync(saleId, products, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();

            var handler = new AddSaleItemCommandHandler(saleServiceMock.Object, notifierMock.Object);

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.Equal(Unit.Value, result);
            saleServiceMock.Verify(s => s.AddSaleItemAsync(saleId, products, It.IsAny<CancellationToken>()), Times.Never);
            notifierMock.Verify(s => s.Handle(It.IsAny<Notification>()), Times.Once);
        }
    }
}
