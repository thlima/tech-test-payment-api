﻿using FluentAssertions;
using Moq;
using Pottencial.Payment.Application.CommandStack.RemoveSaleItem;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.Application.CommandStack.Tests.RemoveSaleItem
{
    public class RemoveSaleItemCommandHandlerTests
    {
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task Handle_WithValidCommand_ShouldReturnIsRemoved(bool isRemoved)
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var productId = Guid.NewGuid();
            var command = new RemoveSaleItemCommand(saleId, productId);

            var saleServiceMock = new Mock<ISaleService>();
            saleServiceMock
                .Setup(s => s.RemoveSaleItemAsync(saleId, productId, It.IsAny<CancellationToken>()))
                .ReturnsAsync(isRemoved)
                .Verifiable();

            var handler = new RemoveSaleItemCommandHandler(saleServiceMock.Object);

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert
            if(isRemoved)
                result.Should().BeTrue();
            else
                result.Should().BeFalse();

            saleServiceMock.Verify(s => s.RemoveSaleItemAsync(saleId, productId, It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
