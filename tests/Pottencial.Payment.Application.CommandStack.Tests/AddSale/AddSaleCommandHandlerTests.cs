﻿using FluentAssertions;
using Moq;
using Pottencial.Payment.CommandStack.AddSale;
using Pottencial.Payment.Domain;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Services;

namespace Pottencial.Payment.Application.CommandStack.Tests.AddSale
{
    public class AddSaleCommandHandlerTests
    {
        private readonly Mock<ISaleService> _saleServiceMock;
        private readonly Mock<INotifier> _notifierMock;
        private readonly AddSaleCommandHandler _handler;

        public AddSaleCommandHandlerTests()
        {
            _saleServiceMock = new Mock<ISaleService>();
            _notifierMock = new Mock<INotifier>();
            _handler = new AddSaleCommandHandler(_saleServiceMock.Object, _notifierMock.Object);
        }

        [Fact]
        public async Task Handle_WhenWithInvalidCommandBecauseProductsIsEmpty_ReturnsNull_And_NotifiesError()
        {
            // Arrange
            var seller = new Seller("Seller Name", "seller@email.com", "12345678912", "1234567");

            var products = new List<Product>();

            var sale = new Sale(seller);

            var command = new AddSaleCommand(sale);

            var handler = new AddSaleCommandHandler(_saleServiceMock.Object, _notifierMock.Object);

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert
            result.Should().Be(null);
            _notifierMock.Verify(x => x.Handle(It.IsAny<Notification>()), Times.Once);
        }

        [Fact]
        public async Task Handle_ShouldReturnAddSaleCommandResponse_WhenSaleIsAddedSuccessfully()
        {
            // Arrange
            var seller = new Seller("Seller Name", "seller@email.com", "12345678912", "1234567");

            var sale = new Sale(seller) { Id = Guid.NewGuid(), SellerId = Guid.NewGuid()};

            var product = new Product("TV"); 
            sale.AddProduct(product);

            var command = new AddSaleCommand(sale);

            _saleServiceMock
                .Setup(s => s.AddSaleAsync(It.IsAny<Sale>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(sale);

            // Act
            var result = await _handler.Handle(command, CancellationToken.None);

            // Assert
            result.Should().NotBeNull();
            result.SaleId.Should().Be(sale.Id);
            result.SellerId.Should().Be(sale.SellerId);

            _saleServiceMock.Verify(s => s.AddSaleAsync(sale, It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
