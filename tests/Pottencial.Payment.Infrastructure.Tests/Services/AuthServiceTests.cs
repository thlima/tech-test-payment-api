﻿using Microsoft.Extensions.Configuration;
using Moq;
using Pottencial.Payment.Infrastructure.Services;
using System.IdentityModel.Tokens.Jwt;

namespace Pottencial.Payment.Infrastructure.Tests.Services
{
    public class AuthServiceTests
    {
        private readonly Mock<IConfiguration> _configurationMock;
        private readonly AuthService _authService;

        public AuthServiceTests()
        {
            _configurationMock = new Mock<IConfiguration>();
            _authService = new AuthService(_configurationMock.Object);
        }

        [Fact]
        public void ComputeSha256Hash_ReturnsCorrectHash()
        {
            // Arrange
            string password = "12345";
            string expectedHash = "5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5";

            // Act
            string result = _authService.ComputeSha256Hash(password);

            // Assert
            Assert.Equal(expectedHash, result);
        }

        [Fact]
        public void GenerateJwtToken_ReturnsToken()
        {
            // Arrange
            string email = "test@example.com";
            _configurationMock.Setup(c => c["Jwt:Issuer"]).Returns("testIssuer");
            _configurationMock.Setup(c => c["Jwt:Audience"]).Returns("testAudience");
            _configurationMock.Setup(c => c["Jwt:Key"]).Returns("testKey12345678901234567890123456789012");

            // Act
            string token = _authService.GenerateJwtToken(email);

            // Assert
            Assert.NotNull(token);

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadJwtToken(token);

            Assert.Equal("testIssuer", jwtToken.Issuer);
            Assert.Equal("testAudience", jwtToken.Audiences.First());
            Assert.Equal(email, jwtToken.Claims.First(c => c.Type == "userName").Value);
        }

        [Fact]
        public void GenerateJwtToken_ThrowsException_WhenIssuerIsNull()
        {
            // Arrange
            string email = "test@example.com";
            _configurationMock.Setup(c => c["Jwt:Issuer"]).Returns((string)null);

            // Act & Assert
            Assert.Throws<NullReferenceException>(() => _authService.GenerateJwtToken(email));
        }
    }
}
