﻿using FluentAssertions;
using Moq;
using Pottencial.Payment.Domain;
using Pottencial.Payment.Domain.Enums;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Repository;
using Pottencial.Payment.Infrastructure.Services;

namespace Pottencial.Payment.Infrastructure.Tests.Services
{
    public class SaleServiceTests
    {
        #region AddSale

        [Fact]
        public async Task AddSaleAsync_SaleAddedSuccessfully_ShouldReturnSale()
        {
            // Arrange
            var sale = new Sale { Id = Guid.NewGuid() };
            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            saleRepositoryMock.Setup(r => r.AddAsync(sale, It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);
            saleRepositoryMock.Setup(r => r.GetByIdAsync(sale.Id, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            var result = await service.AddSaleAsync(sale, CancellationToken.None);

            // Assert
            result.Should().NotBeNull();
            result.Should().Be(sale);
        }

        [Fact]
        public async Task AddSaleAsync_SaleNotFound_ShouldNotifyError()
        {
            // Arrange
            var sale = new Sale { Id = Guid.NewGuid() };
            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            saleRepositoryMock.Setup(r => r.AddAsync(sale, It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);
            saleRepositoryMock.Setup(r => r.GetByIdAsync(sale.Id, It.IsAny<CancellationToken>())).ReturnsAsync((Sale)null);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            await service.AddSaleAsync(sale, CancellationToken.None);

            // Assert
            notifierMock.Verify(notifier => notifier.Handle(It.IsAny<Notification>()), Times.Once());
        }

        #endregion

        #region AddSaleItem

        [Fact]
        public async Task AddSaleItemAsync_SaleExistsAndStatusIsAwaitingPayment_ShouldAddItems()
        {
            // Arrange
            Sale sale;
            List<Product> products;

            SetupSale(out sale, out products);

            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            notifierMock.Setup(x => x.GetNotifications()).Returns(new List<Notification>());
            saleRepositoryMock.Setup(r => r.GetByIdAsync(sale.Id, It.IsAny<CancellationToken>())).ReturnsAsync(sale);
            saleRepositoryMock.Setup(r => r.AddItemSaleAsync(sale, products, It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            await service.AddSaleItemAsync(sale.Id, products, CancellationToken.None);

            // Assert
            saleRepositoryMock.Verify(r => r.AddItemSaleAsync(sale, products, It.IsAny<CancellationToken>()), Times.Once);
        }

        [Fact]
        public async Task AddSaleItemAsync_SaleNotFound_ShouldNotifyError()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            Sale sale;
            var products = new List<Product>
            {
                new Product("TV")
                {
                    Id = Guid.NewGuid()
                }
            };

            SetupSale(out sale, out products);

            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            saleRepositoryMock.Setup(r => r.GetByIdAsync(saleId, It.IsAny<CancellationToken>())).ReturnsAsync((Sale)null);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            await service.AddSaleItemAsync(saleId, products, CancellationToken.None);

            // Assert
            notifierMock.Verify(notifier => notifier.Handle(It.IsAny<Notification>()), Times.Once());
        }

        [Fact]
        public async Task AddSaleItemAsync_SaleNotInAwaitingPaymentStatus_ShouldNotify()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var products = new List<Product>
            {
                new Product
                {
                    Id = Guid.NewGuid()
                }
            };

            var sale = new Sale
            {
                Id = saleId
            };

            typeof(Sale).GetProperty("Status").SetValue(sale, SaleStatus.Delivered);

            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            notifierMock.Setup(x => x.GetNotifications()).Returns(new List<Notification>());
            saleRepositoryMock.Setup(r => r.GetByIdAsync(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            await service.AddSaleItemAsync(saleId, products, CancellationToken.None);

            // Assert
            notifierMock.Verify(n => n.Handle(It.Is<Notification>(notif => notif.Message == "It is not possible to add items to a sale that is not in 'Awaiting Payment' status.")), Times.Once);
        }

        #endregion

        #region UpdateSaleStatus

        [Fact]
        public async Task UpdateSaleAsync_ValidStatusWhenChangeToPaymentAccept_ReturnsTrue()
        {
            // Arrange
            var seller = new Seller("Seller Name", "seller@email.com", "12345678912", "1234567");
            var products = new List<Product>()
            {
                new ("TV"),
                new ("PS5"),
                new ("Headset"),
            };

            var sale = new Sale(seller);

            var newStatus = SaleStatus.PaymentAccept;

            var cancellationToken = new CancellationToken();
            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            saleRepositoryMock.Setup(repo => repo.GetByIdAsync(sale.Id, cancellationToken))
                              .ReturnsAsync(sale);

            saleRepositoryMock.Setup(repo => repo.UpdateAsync(sale, cancellationToken))
                              .Returns(Task.CompletedTask);

            var saleService = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            var result = await saleService.UpdateSaleStatusAsync(sale.Id, newStatus, cancellationToken);

            // Assert
            result.Should().BeTrue();
            notifierMock.Verify(notifier => notifier.Handle(It.IsAny<Notification>()), Times.Never());
        }

        [Fact]
        public async Task UpdateSaleStatusAsync_SaleNotFound_ShouldNotifyAndReturnFalse()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var newStatus = SaleStatus.Delivered;

            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            saleRepositoryMock.Setup(r => r.GetByIdAsync(saleId, It.IsAny<CancellationToken>())).ReturnsAsync((Sale)null);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            var result = await service.UpdateSaleStatusAsync(saleId, newStatus, CancellationToken.None);

            // Assert
            result.Should().BeFalse();
            notifierMock.Verify(n => n.Handle(It.Is<Notification>(notif => notif.Message == "Sale was not found!")), Times.Once);
        }

        [Fact]
        public async Task UpdateSaleStatusAsync_StatusUpdateIsInvalid_ShouldNotifyAndReturnFalse()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var newStatus = SaleStatus.SentToCarrier;
            var sale = new Sale
            {
                Id = saleId
            };

            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            saleRepositoryMock.Setup(r => r.GetByIdAsync(saleId, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            var result = await service.UpdateSaleStatusAsync(saleId, newStatus, CancellationToken.None);

            // Assert
            result.Should().BeFalse();
            notifierMock.Verify(n => n.Handle(It.Is<Notification>(notif => notif.Message == $"Cannot change status from {sale.Status} to {newStatus}")), Times.Once);
        }

        #endregion

        #region RemoveSaleItem

        [Fact]
        public async Task RemoveSaleItemAsync_SaleAndProductExistAndSaleStatusIsAwaitingPayment_ShouldRemoveProduct()
        {
            // Arrange
            Sale sale;
            List<Product> products;

            SetupSale(out sale, out products);

            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            notifierMock.Setup(x => x.GetNotifications()).Returns(new List<Notification>());
            saleRepositoryMock.Setup(r => r.GetByIdAsync(sale.Id, It.IsAny<CancellationToken>())).ReturnsAsync(sale);
            saleRepositoryMock.Setup(r => r.RemoveProductAsync(sale, products[0], It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            var result = await service.RemoveSaleItemAsync(sale.Id, products[0].Id, CancellationToken.None);

            // Assert
            result.Should().BeTrue();
            saleRepositoryMock.Verify(r => r.RemoveProductAsync(sale, products[0], It.IsAny<CancellationToken>()), Times.Once);
        }

        [Fact]
        public async Task RemoveSaleItemAsync_SaleNotFound_ShouldNotifyError()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            var productId = Guid.NewGuid();

            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            saleRepositoryMock.Setup(r => r.GetByIdAsync(saleId, It.IsAny<CancellationToken>())).ReturnsAsync((Sale)null);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            var result = await service.RemoveSaleItemAsync(saleId, productId, CancellationToken.None);

            // Assert
            result.Should().BeFalse();
            notifierMock.Verify(notifier => notifier.Handle(It.IsAny<Notification>()), Times.Once());
        }

        [Fact]
        public async Task RemoveSaleItemAsync_ProductNotFound_ShouldNotifyError()
        {
            // Arrange
            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            notifierMock.Setup(x => x.GetNotifications()).Returns(new List<Notification>());
            saleRepositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync((Sale)null);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            var result = await service.RemoveSaleItemAsync(It.IsAny<Guid>(), It.IsAny<Guid>(), CancellationToken.None);

            // Assert
            result.Should().BeFalse();
            notifierMock.Verify(notifier => notifier.Handle(It.IsAny<Notification>()), Times.Once());
        }

        [Fact]
        public async Task RemoveSaleItemAsync_SaleNotInAwaitingPaymentStatus_ShouldNotifyAndReturnFalse()
        {
            // Arrange
            var expectedErrorMessage = "It is not possible to remove items to a sale that is not in 'Awaiting Payment' status.";

            Sale sale;
            List<Product> products;

            SetupSale(out sale, out products);

            typeof(Sale).GetProperty("Status").SetValue(sale, SaleStatus.PaymentAccept);

            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            notifierMock.Setup(x => x.GetNotifications()).Returns(new List<Notification>()
            {
                new Notification(expectedErrorMessage)
            });
            saleRepositoryMock.Setup(r => r.GetByIdAsync(sale.Id, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            var result = await service.RemoveSaleItemAsync(sale.Id, products[0].Id, CancellationToken.None);

            // Assert
            result.Should().BeFalse();
            notifierMock.Verify(n => n.Handle(It.Is<Notification>(notif => notif.Message == expectedErrorMessage)), Times.Once);
        }

        [Fact]
        public async Task RemoveSaleItemAsync_SaleWithOneProduct_MustNotifyErrorAndNotRemoveProduct()
        {
            // Arrange
            var expectedErrorMessage = "It is not possible to remove this product, as a sale must have at least one product";

            Sale sale;
            List<Product> products;

            var seller = new Seller("Seller Name", "seller@email.com", "12345678912", "1234567");
            sale = new Sale(seller) { Id = Guid.NewGuid(), SellerId = Guid.NewGuid() };
            products = new List<Product>
            {
                new Product("TV"),
            };

            foreach (var product in products)
            {
                sale.AddProduct(product);
            }

            typeof(Sale).GetProperty("Status").SetValue(sale, SaleStatus.PaymentAccept);

            var saleRepositoryMock = new Mock<ISaleRepository>();
            var notifierMock = new Mock<INotifier>();

            notifierMock.Setup(x => x.GetNotifications()).Returns(new List<Notification>()
            {
                new Notification(expectedErrorMessage)
            });
            saleRepositoryMock.Setup(r => r.GetByIdAsync(sale.Id, It.IsAny<CancellationToken>())).ReturnsAsync(sale);

            var service = new SaleService(saleRepositoryMock.Object, notifierMock.Object);

            // Act
            var result = await service.RemoveSaleItemAsync(sale.Id, products[0].Id, CancellationToken.None);

            // Assert
            result.Should().BeFalse();
            notifierMock.Verify(n => n.Handle(It.Is<Notification>(notif => notif.Message == expectedErrorMessage)), Times.Once);
        }

        #endregion

        private static void SetupSale(out Sale sale, out List<Product> products)
        {
            var seller = new Seller("Seller Name", "seller@email.com", "12345678912", "1234567");
            sale = new Sale(seller) { Id = Guid.NewGuid(), SellerId = Guid.NewGuid() };
            products = new List<Product>
            {
                new Product("TV"),
                new Product("PS5"),
            };
            foreach (var product in products)
            {
                sale.AddProduct(product);
            }
        }
    }
}
