﻿using FluentAssertions;
using Pottencial.Payment.Domain.Enums;

namespace Pottencial.Payment.Domain.Tests
{
    public class SaleTests
    {
        [Theory]
        [InlineData(SaleStatus.AwaitingPayment, SaleStatus.PaymentAccept, true)]
        [InlineData(SaleStatus.AwaitingPayment, SaleStatus.Canceled, true)]
        [InlineData(SaleStatus.PaymentAccept, SaleStatus.SentToCarrier, true)]
        [InlineData(SaleStatus.PaymentAccept, SaleStatus.Canceled, true)]
        [InlineData(SaleStatus.SentToCarrier, SaleStatus.Delivered, true)]
        [InlineData(SaleStatus.AwaitingPayment, SaleStatus.SentToCarrier, false)]
        [InlineData(SaleStatus.PaymentAccept, SaleStatus.AwaitingPayment, false)]
        [InlineData(SaleStatus.SentToCarrier, SaleStatus.PaymentAccept, false)]
        [InlineData(SaleStatus.SentToCarrier, SaleStatus.Canceled, false)]
        [InlineData(SaleStatus.Delivered, SaleStatus.AwaitingPayment, false)]
        public void ValidateAndUpdateSaleStatus_ValidatesTransitionsOfSaleStatusCorrectly(SaleStatus currentStatus, SaleStatus newStatus, bool expectedResult)
        {
            // Arrange
            var sale = new Sale();

            typeof(Sale).GetProperty("Status").SetValue(sale, currentStatus);

            // Act
            var result = sale.ValidateAndUpdateSaleStatus(currentStatus, newStatus);

            // Assert
            var expected = expectedResult ? newStatus : currentStatus;

            result.Should().Be(expectedResult);
            Assert.Equal(expected, sale.Status);
        }
    }
}
