﻿using FluentAssertions;
using Moq;
using Pottencial.Payment.Application.QueryStack.GetSaleById;
using Pottencial.Payment.Domain;
using Pottencial.Payment.Domain.Notifications;
using Pottencial.Payment.Domain.Repository;

namespace Pottencial.Payment.Application.QuerySatck.Tests.GetSaleById
{
    public class GetSaleByIdQueryHandlerTests
    {
        [Fact]
        public async Task Handle_SaleExists_ShouldReturnSaleWithProducts()
        {
            // Arrange
            var seller = new Seller("Seller Name", "seller@email.com", "12345678912", "1234567");
            var sale = new Sale(seller) { Id = Guid.NewGuid(), SellerId = Guid.NewGuid() };

            var products = new List<Product>
            {
                new Product("TV"),
                new Product("PS5"),
            };

            foreach (var product in products) 
            {
                sale.AddProduct(product);
            }

            var saleRepositoryMock = new Mock<ISaleRepository>();
            saleRepositoryMock
                .Setup(s => s.GetByIdAsync(sale.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(sale);

            saleRepositoryMock
                .Setup(s => s.GetAllProductsBySaleId(sale.Id))
                .ReturnsAsync(products);

            var notifierMock = new Mock<INotifier>();

            var handler = new GetSaleByIdQueryHandler(saleRepositoryMock.Object, notifierMock.Object);
            var query = new GetSaleByIdQuery(sale.Id);

            // Act
            var result = await handler.Handle(query, CancellationToken.None);

            // Assert
            result.Should().NotBeNull();
            result.SellerName.Should().Be(sale.Seller.Name);
            result.SaleDate.Should().Be(sale.SaleDate);
            result.Status.Should().Be(sale.Status.ToString());
            result.Products.Should().HaveCount(2);
            result.Products[0].Description.Should().Be(products[0].Description);
            result.Products[1].Description.Should().Be(products[1].Description);
        }

        [Fact]
        public async Task Handle_SaleDoesNotExist_ShouldNotifyAndReturnNull()
        {
            // Arrange
            var saleId = Guid.NewGuid();

            var saleRepositoryMock = new Mock<ISaleRepository>();

            saleRepositoryMock
                .Setup(s => s.GetByIdAsync(saleId, It.IsAny<CancellationToken>()))
                .ReturnsAsync((Sale)null);

            var notifierMock = new Mock<INotifier>();

            notifierMock
                .Setup(n => n.Handle(It.IsAny<Notification>()))
                .Verifiable();

            var handler = new GetSaleByIdQueryHandler(saleRepositoryMock.Object, notifierMock.Object);
            var query = new GetSaleByIdQuery(saleId);

            // Act
            var result = await handler.Handle(query, CancellationToken.None);

            // Assert
            result.Should().BeNull();
            notifierMock.Verify(n => n.Handle(It.Is<Notification>(notif => notif.Message == "Sale was not found!")), Times.Once);
        }
    }
}
